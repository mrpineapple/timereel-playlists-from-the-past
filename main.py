import json
from pprint import pprint
import urllib2,urllib
from xml.dom.minidom import parse, parseString
import os

#clear directories
os.system("rm timereel/*")

#open our charts file
with open('mtv.csv') as f:
    content = f.readlines()

data = {}

count = 0.0
for line in content:        #[990:]:
    details = line.split(',')
    print details[9] + " by " + details[10] + " : " + details[11]

    spotify_url = 'http://ws.spotify.com/search/1/track?q='+urllib.quote_plus(details[9])    # space to %20, etc
    response = urllib2.urlopen(spotify_url)

    html = response.read()
    xmldoc = parseString(html)
    try:
        elem = xmldoc.getElementsByTagName('track')[0]#.attrib("href") 
        href = elem.getAttributeNode("href")
        uri = href.value
        uri = uri.split(":")[2]
        if data.has_key(str(details[11])):
            data[str(details[11])].append(uri)
        else:
            data[str(details[11])] = [uri]
    except IndexError:
        print "Unable to be found"

for x in data:
    x = x.replace(" ","")
    f = open('timereel/'+x+'.html','a+')
    uris = ""
    for y in data[x]:
        if uris == "":
            uris = y
        else:
            uris = uris + "," + y

    print uris
    f.write('<html><body style="text-align: center;"><iframe src="https://embed.spotify.com/?uri=spotify:trackset:'+x+':'+uris+'" width="300" height="380" frameborder="0" allowtransparency="true"></iframe></body>') # python will convert \n to os.linesep
    # f.close()
    # count = count + 1
    # print str(count/10) + "%"
